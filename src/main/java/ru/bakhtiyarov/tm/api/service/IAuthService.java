package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void checkRole(Role[] roles);

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
