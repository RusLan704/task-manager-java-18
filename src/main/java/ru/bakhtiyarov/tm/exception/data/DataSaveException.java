package ru.bakhtiyarov.tm.exception.data;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class DataSaveException extends AbstractException {

    public DataSaveException() {
        super("An error occurred while saving data");
    }

}
