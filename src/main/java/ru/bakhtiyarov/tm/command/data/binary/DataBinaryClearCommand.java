package ru.bakhtiyarov.tm.command.data.binary;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-clear";
    }

    @Override
    public String description() {
        return "Clear bin file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN CLEAR]");
        final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}


