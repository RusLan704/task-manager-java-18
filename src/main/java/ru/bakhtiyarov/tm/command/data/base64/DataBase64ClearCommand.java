package ru.bakhtiyarov.tm.command.data.base64;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public String description() {
        return "Clear base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
