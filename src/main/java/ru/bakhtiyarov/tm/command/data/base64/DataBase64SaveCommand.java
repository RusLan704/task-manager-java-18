package ru.bakhtiyarov.tm.command.data.base64;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataSaveException;
import sun.misc.BASE64Encoder;

import javax.imageio.IIOException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-base64-save";
    }

    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        try (
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        ) {
            objectOutputStream.writeObject(domain);
            final byte[] bytes = byteArrayOutputStream.toByteArray();
            final String base64 = new BASE64Encoder().encode(bytes);
            fileOutputStream.write(base64.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
