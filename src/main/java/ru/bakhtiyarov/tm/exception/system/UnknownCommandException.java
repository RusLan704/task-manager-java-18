package ru.bakhtiyarov.tm.exception.system;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}