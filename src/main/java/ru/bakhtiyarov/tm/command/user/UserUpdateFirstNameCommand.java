package ru.bakhtiyarov.tm.command.user;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class UserUpdateFirstNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_FIRST_NAME;
    }

    @Override
    public String description() {
        return "Update user first name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE FIRST NAME]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateUserFirstName(userId, firstName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}