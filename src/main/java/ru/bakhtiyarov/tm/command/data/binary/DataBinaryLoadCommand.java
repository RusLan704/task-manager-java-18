package ru.bakhtiyarov.tm.command.data.binary;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataLoadException;

import javax.imageio.IIOException;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        try (
                final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
                final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}