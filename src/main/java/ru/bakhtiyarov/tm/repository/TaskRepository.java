package ru.bakhtiyarov.tm.repository;

import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void addAll(List<Task> tasks) {
        this.tasks.addAll(tasks);
    }

    public void add(final Collection<Task> tasks) {
        for (final Task task : tasks) {
            this.tasks.addAll(tasks);
        }
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks)
            if (userId.equals(task.getUserId())) result.add(task);
        return result;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        final Task task = tasks.get(index);
        if (userId.equals(task.getUserId())) return task;
        return null;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) continue;
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}
