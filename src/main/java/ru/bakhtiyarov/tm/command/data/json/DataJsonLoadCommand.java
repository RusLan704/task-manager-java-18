package ru.bakhtiyarov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataLoadException;

import javax.imageio.IIOException;
import java.io.FileInputStream;

public class DataJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        try (
                final FileInputStream fileInputStream = new FileInputStream(FILE_JSON)
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}