package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void addAll(List<Task> tasks);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    List<Task> findAll();

    void clear(String userId);

    void clear();

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task removeOneByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

}
