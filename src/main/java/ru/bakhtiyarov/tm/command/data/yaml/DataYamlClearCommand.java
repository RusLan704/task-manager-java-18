package ru.bakhtiyarov.tm.command.data.yaml;

import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataYamlClearCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-yaml-clear";
    }

    @Override
    public String description() {
        return "Clear yaml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML CLEAR]");
        final File file = new File(FILE_YAML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}