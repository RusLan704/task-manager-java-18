package ru.bakhtiyarov.tm.exception.data;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class DataLoadException extends AbstractException {

    public DataLoadException() {
        super("An error occurred while loading data");
    }

}
