package ru.bakhtiyarov.tm.repository;

import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.command.auth.LoginCommand;
import ru.bakhtiyarov.tm.command.auth.LogoutCommand;
import ru.bakhtiyarov.tm.command.auth.RegistryCommand;
import ru.bakhtiyarov.tm.command.data.base64.DataBase64ClearCommand;
import ru.bakhtiyarov.tm.command.data.base64.DataBase64LoadCommand;
import ru.bakhtiyarov.tm.command.data.binary.DataBinaryClearCommand;
import ru.bakhtiyarov.tm.command.data.binary.DataBinaryLoadCommand;
import ru.bakhtiyarov.tm.command.data.binary.DataBinarySaveCommand;
import ru.bakhtiyarov.tm.command.data.json.DataJsonClearCommand;
import ru.bakhtiyarov.tm.command.data.json.DataJsonLoadCommand;
import ru.bakhtiyarov.tm.command.data.json.DataJsonSaveCommand;
import ru.bakhtiyarov.tm.command.data.xml.DataXmlClearCommand;
import ru.bakhtiyarov.tm.command.data.xml.DataXmlLoadCommand;
import ru.bakhtiyarov.tm.command.data.xml.DataXmlSaveCommand;
import ru.bakhtiyarov.tm.command.data.yaml.DataYamlClearCommand;
import ru.bakhtiyarov.tm.command.data.yaml.DataYamlLoadCommand;
import ru.bakhtiyarov.tm.command.data.yaml.DataYamlSaveCommand;
import ru.bakhtiyarov.tm.command.project.*;
import ru.bakhtiyarov.tm.command.system.*;
import ru.bakhtiyarov.tm.command.task.*;
import ru.bakhtiyarov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public final class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            HelpCommand.class, InfoCommand.class, AboutCommand.class, VersionCommand.class,
            ShowCommand.class, ArgumentsCommand.class, ExitCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class, TaskListCommand.class, TaskRemoveByIdCommand.class,
            TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskUpdateByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskViewByIdCommand.class, TaskViewByIndexCommand.class,
            TaskViewByNameCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
            ProjectRemoveByIdCommand.class, ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,
            ProjectViewByIdCommand.class, ProjectViewByIndexCommand.class, ProjectViewByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class,

            UserViewProfileCommand.class, UserUpdateLoginCommand.class, UserUpdatePasswordCommand.class,
            UserUpdateFirstNameCommand.class, UserUpdateMiddleNameCommand.class, UserUpdateLastNameCommand.class,
            UserUpdateEmailCommand.class,

            LoginCommand.class, LogoutCommand.class, RegistryCommand.class, UserLockCommand.class,
            UserUnlockCommand.class, UserRemoveByLoginCommand.class, UserRemoveByIdCommand.class,

            DataBase64LoadCommand.class, DataBase64LoadCommand.class, DataBase64ClearCommand.class,
            DataBinaryLoadCommand.class, DataBinarySaveCommand.class, DataBinaryClearCommand.class,
            DataXmlSaveCommand.class, DataXmlLoadCommand.class, DataXmlClearCommand.class,
            DataJsonSaveCommand.class, DataJsonLoadCommand.class, DataJsonClearCommand.class,
            DataYamlSaveCommand.class, DataYamlLoadCommand.class, DataYamlClearCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}