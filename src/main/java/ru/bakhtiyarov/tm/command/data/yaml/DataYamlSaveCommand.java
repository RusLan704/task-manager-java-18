package ru.bakhtiyarov.tm.command.data.yaml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.exception.data.DataSaveException;

import javax.imageio.IIOException;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataYamlSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-yaml-save";
    }

    @Override
    public String description() {
        return "Save data to yaml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_YAML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        YAMLFactory yamlFactory = new YAMLFactory();
        final ObjectMapper objectMapper = new ObjectMapper(yamlFactory);
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(FILE_YAML);
        ) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException();
        }
        System.out.println("[OK]");
    }
}
