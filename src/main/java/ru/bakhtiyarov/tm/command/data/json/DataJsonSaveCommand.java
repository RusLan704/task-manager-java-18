package ru.bakhtiyarov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataSaveException;

import javax.imageio.IIOException;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataJsonSaveCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-json-save";
    }

    @Override
    public String description() {
        return "Save data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        ) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IIOException e) {
            throw new DataSaveException();
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
